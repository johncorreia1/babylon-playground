export const mockHotspots = [
  {
    id: "123abc",
    name: "hotspot-npc-1",
    position: { x: -5, y: 2.8, z: -3.5 },
    content: {
      title: "Spartan Jameson Locke",
      type: "text",
      body: (
        <div>
          Locke was born on March 15, 2529, on Jericho VII. When he was six
          years old, Jericho VII fell to the Covenant Empire and Locke was
          evacuated. His parents were killed during the assault.
          <br />
          <br />
          Locke eventually joined the UNSC and became an ONI Acquisitions
          Specialist, reaching the rank of Lieutenant Commander.
          <br />
          <br />
          On September 26, 2552, Locke compiled a target profile report
          regarding Thel 'Vadam's personal history during the war, including the
          Fall of Reach, the Battle of Installation 04, and the Great Schism.
        </div>
      ),
    },
  },
];
