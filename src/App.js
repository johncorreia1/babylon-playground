import { useCallback, useState } from "react";
import { Route, Switch } from "react-router-dom";
import Layout from "./components/Layout";
import BabylonCanvas from "./3d/BabylonCanvas";
import {mockHotspots} from './utils/mockData';
import "./App.css";

function App() {
  const [showModal, setShowModal] = useState({ display: false, hotspot: {} });

  const handleShowModal = useCallback(
    (hostpotId) => {
      let selectedHostpot = mockHotspots.find((hotspot) =>
        hostpotId.includes(hotspot.id)
      );
      if (selectedHostpot) {
        console.log(selectedHostpot);
        setShowModal({ display: true, hotspot: selectedHostpot });
      }
    },
    [setShowModal]
  );
  return (
    <div className="App">
      <Layout showModal={showModal} setShowModal={setShowModal} />
      <Switch>
        <Route path="/" exact>
          <BabylonCanvas scene={"main"} hotspots={mockHotspots} setShowModal={handleShowModal} />
        </Route>
        <Route path="/overview" exact>
          <BabylonCanvas scene={"overview"} />
        </Route>
        <Route path="/blank" exact>
          <BabylonCanvas scene={"blank"} />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
