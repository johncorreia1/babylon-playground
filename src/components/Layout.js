import LoadingScreen from "./Loading";

const Layout = ({ showModal, setShowModal }) => {
  const { hotspot } = showModal;
  return (
    <div>
      {showModal.display && (
        <div
          style={{
            width: "100vw",
            height: "100vh",
            position: "absolute",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div
            style={{
              width: "400px",
              minHeight: "300px",
              backgroundColor: "#ececf3",
              borderRadius: "20px",
              padding: "20px",
            }}
          >
            <div
              style={{
                textAlign: "center",
                fontSize: "24px",
                fontWeight: "bold",
              }}
            >
              {hotspot.content.title || "Default Modal Title"}
            </div>
            <div
              style={{
                textAlign: "left",
                fontSize: "16px",
                marginTop: "16px",
              }}
            >
              {hotspot.content.body || 'Defaut Modal Content'}
            </div>
            <button
              onClick={() => setShowModal({ display: false, hotspot: {} })}
              style={{
                textAlign: "center",
                fontSize: "18px",
                marginTop: "16px",
              }}
            >
              {hotspot.content.button || 'Close'}
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Layout;
