import {
  Vector3,
  SceneLoader,
  HemisphericLight,
  ArcRotateCamera,
} from "@babylonjs/core";
import "@babylonjs/inspector";
import "@babylonjs/loaders/glTF";
import hotspotGenerator from "../scripts/hotspot";

const overviewScene = (scene) => {
  const canvas = scene.getEngine().getRenderingCanvas();

  var camera = new ArcRotateCamera(
    "Camera",
    4.35,
    1.24,
    10,
    new Vector3(0, 0, 0),
    scene
  );
  camera.setPosition(new Vector3(0, 0, 20));
  camera.attachControl(canvas, true);
  var light = new HemisphericLight("light", new Vector3(0, 1, 0), scene);
  light.intensity = 1.2;
  SceneLoader.ImportMesh(
    "",
    "https://experience.xureal.com/babylon/static/media/Tour_Baked.gltf",
    "",
    scene,
    (res) => {
      res[0].position = new Vector3(-1, -2, -1);
      res[0].rotation = new Vector3(0, 0, 0);
      res[0].scaling = new Vector3(0.1, 0.1, -0.1);
    }
  );
  hotspotGenerator(scene)
};

export default overviewScene;
