import {
  Vector3,
  SceneLoader,
  MeshBuilder,
  Mesh,
  HemisphericLight,
  StandardMaterial,
} from "@babylonjs/core";
import "@babylonjs/inspector";
import "@babylonjs/loaders/glTF";
import assetManager from "../scripts/assetManager";

import cameraSetup from "../scripts/camera";
import hotspotSetup from "../scripts/hotspot";
import StoreModel from "../../assets/store.gltf";
import RoomModel from "../../assets/room.gltf";
import SpartanModel from '../../assets/spartan.glb'
import QuestionBlockModel from '../../assets/question-block.glb'

const mainScene = async (scene, setShowModal, hotspots) => {
  const canvas = scene.getEngine().getRenderingCanvas();
  const degToRad = (deg) => deg * Math.PI / 180

  const light = new HemisphericLight("light", new Vector3(0, 2, 5), scene);
  light.intensity = 1;

  //Add the camera systems
  cameraSetup(scene, canvas, setShowModal);

  // Load assets via asset manager
  assetManager(scene, [
    {
      name: "room",
      model: RoomModel,
      position: new Vector3(-12, 0, -1.75),
      rotation: new Vector3(0, degToRad(180), 0),
      scaling: new Vector3(0.85, 0.85, 0.85),
    },
    {
      name: "store",
      model: StoreModel,
      position: new Vector3(0, 0, 0),
      rotation: new Vector3(0, 0, 0),
      scaling: new Vector3(-1, 1, 1),
    },
    {
      name: 'spartan',
      model: SpartanModel,
      position: new Vector3(-5, 0, -3.5),
      rotation: new Vector3(0, degToRad(45), 0),
      scaling: new Vector3(0.325, 0.325, 0.325),
    },
    {
      name: 'question-block',
      model: QuestionBlockModel,
      position: new Vector3(-5, 0, -3.5),
    }
  ]);

  //Ground
  var ground = MeshBuilder.CreateGround(
    "ground",
    { width: 100, height: 100, minHeight: 0.1 },
    scene
  );
  ground.material = new StandardMaterial("groundMat", scene);
  ground.position.y = 0.2;
  ground.material.alpha = 0;
  ground.material.backFaceCulling = false;
  ground.checkCollisions = true;


  /* End Create Scenery */

  // scene.debugLayer.show({
  //   embedMode: true,
  // });
  hotspotSetup(scene, setShowModal, hotspots)
  //Gravity and Collisions Enabled
  scene.gravity = new Vector3(0, -0.9, 0);
  scene.collisionsEnabled = true;
};

export default mainScene;
