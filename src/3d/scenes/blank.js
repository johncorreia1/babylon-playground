import {
  Vector3,
  HemisphericLight,
  MeshBuilder,
  StandardMaterial,
  SceneLoader,
  PointerEventTypes,
  // Texture,
} from "@babylonjs/core";
import "@babylonjs/inspector";
import "@babylonjs/loaders/glTF";
import cameraSetup from "../scripts/camera";
import hotspotGenerator from "../scripts/hotspot";

const blankScene = (scene, setShowModal) => {
  const canvas = scene.getEngine().getRenderingCanvas();
  cameraSetup(scene, canvas);

  const ground = MeshBuilder.CreateGround(
    "ground",
    { width: 100, height: 100 },
    scene
  );

  const groundMat = new StandardMaterial("groundMat", scene);
  ground.material = groundMat;
  ground.material.alpha = 0;
  ground.checkCollisions = true;

  let light = new HemisphericLight("light", new Vector3(0, 1, 0), scene);
  light.intensity = 2;
  SceneLoader.ImportMesh(
    "",
    "https://experience.xureal.com/babylon/static/media/Tour_Baked.gltf",
    "",
    scene,
    (res) => {
      res[0].position = new Vector3(0, -1, 0);
      res[0].rotation = new Vector3(0, 0, 0);
      res[0].scaling = new Vector3(0.5, 0.5, -0.5);
    }
  );
  hotspotGenerator(scene);

  scene.onPointerObservable.add((pointerInfo) => {
    switch (pointerInfo.type) {
      case PointerEventTypes.POINTERDOWN:
        if (pointerInfo.pickInfo.hit) {
          if (pointerInfo.pickInfo.pickedMesh.name.includes("hotspot")) {
            console.log("clicked", pointerInfo.pickInfo.pickedMesh);
            if (setShowModal) setShowModal((prev) => true);
          }
          // pointerDown(pointerInfo.pickInfo.pickedMesh);
        }
        break;
      default:
        break;
    }
  });
};

export default blankScene;
