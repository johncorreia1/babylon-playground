import React, { useCallback } from "react";

// import SceneComponent from "./SceneComponent"; // uses above component in same directory
import SceneComponent from "babylonjs-hook"; // if you install 'babylonjs-hook' NPM.
import overviewScene from "./scenes/overview";
import mainScene from "./scenes/main";
import blankScene from "./scenes/blank";

const BabylonCanvas = ({ scene: selectedScene, setShowModal, hotspots }) => {
  const onSceneReady = useCallback(
    (scene) => {
      console.log("selectedScene", selectedScene);

      scene.debugLayer.show({
        embedMode: true,
      });
      switch (selectedScene) {
        case "main":
          return mainScene(scene, setShowModal, hotspots);
        case "overview":
          return overviewScene(scene);
        case "blank":
          return blankScene(scene);
        default:
          return mainScene(scene);
      }
    },
    [selectedScene, setShowModal, hotspots]
  );

  const onRender = (scene) => {};

  const renderScene = useCallback(() => {
    return (
      <SceneComponent
        antialias
        onSceneReady={onSceneReady}
        onRender={onRender}
        adaptToDeviceRatio={"true"}
        id="my-canvas"
      />
    );
  }, [onSceneReady]);

  return <>{renderScene()}</>;
};

export default BabylonCanvas;
