import {
  MeshBuilder,
  Mesh,
  StandardMaterial,
  Texture,
  Vector3,
  PointerEventTypes,
} from "@babylonjs/core";
import HostpotImage from "../../assets/icon-external.png";
const hotspotGenerator = (scene, setShowModal, hotspots) => {
  const hotspotParent = new Mesh("hotspots", scene);
  scene.onPointerObservable.add((pointerInfo) => {
    switch (pointerInfo.type) {
      case PointerEventTypes.POINTERDOWN:
        if (pointerInfo.pickInfo.hit) {
          if (pointerInfo.pickInfo.pickedMesh.name.includes("hotspot")) {
            console.log("clicked", pointerInfo.pickInfo.pickedMesh);
            // pass in hotspot name here
            if (setShowModal)
              setShowModal(pointerInfo.pickInfo.pickedMesh.name);
          }
          // pointerDown(pointerInfo.pickInfo.pickedMesh);
        }
        break;
      default:
        break;
    }
  });

  const hotspotPlaneOptions = {
    size: 1,
    width: 1,
    height: 1,
  };

  const mat = new StandardMaterial("hotspot-mat", scene);
  const t = new Texture(HostpotImage, scene);
  t.hasAlpha = true;
  mat.diffuseTexture = t;
  mat.useAlphaFromDiffuseTexture = true;
  let i = 0;

  const plane = MeshBuilder.CreatePlane(``, hotspotPlaneOptions, scene);
  plane.material = mat;

  plane.isVisible = false;

  // iterate through hotspot array
  // assign hotspot id to mesh name
  if (!hotspots || hotspots.length < 1) {
    for (i; i < 1000; i++) {
      const newInstance = plane.createInstance(`hotspot-${i}`);
      newInstance.position = new Vector3(
        i % 5 === 0
          ? Math.floor(Math.random() * 25)
          : Math.floor(Math.random() * 25) * -25,
        1,
        Math.floor(Math.random() * 25),
        i % 5 === 0
          ? Math.floor(Math.random() * 25)
          : Math.floor(Math.random() * 25) * -25
      );
      newInstance.billboardMode = Mesh.BILLBOARDMODE_ALL;
    }
  }
  if (hotspots && hotspots.length > 0) {
    for (const hotspot of hotspots) {
      const newInstance = plane.createInstance(`hotspot-${hotspot.id}`);
      newInstance.position = new Vector3(
        hotspot.position.x,
        hotspot.position.y,
        hotspot.position.z
      );
      newInstance.scaling = new Vector3(0.7, 0.7, 0.7);
      newInstance.billboardMode = Mesh.BILLBOARDMODE_Y;
      newInstance.parent = hotspotParent;
    }
  }
};

export default hotspotGenerator;
