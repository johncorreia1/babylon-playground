import { AssetsManager, Vector3 } from "@babylonjs/core";

const assetManager = (scene, assetsToLoad) => {
  // let completedAssets = 0;
  const assetsManager = new AssetsManager(scene);
  const assetTasks = {};
  for (const [i, asset] of assetsToLoad.entries()) {
    console.log(asset);
    const origin = new Vector3(0, 0, 0);
    assetTasks[asset.name] = assetsManager.addMeshTask(
      `${asset.name} task`,
      "",
      asset.model,
      ""
    );

    assetTasks[asset.name].onSuccess = function (task) {
      // completedAssets++;
      console.log("setup mesh", asset.position, asset.rotation, asset.scaling);
      task.loadedMeshes[0].position = asset.position || origin;
      task.loadedMeshes[0].rotation = asset.rotation || origin;
      task.loadedMeshes[0].scaling = asset.scaling || origin;

      // temp filtering for models
      if (asset.name === "store") {
        task.loadedMeshes.filter(
          (m) => m.name === "Sphere"
        )[0].isVisible = false;
        task.loadedMeshes.filter(
          (m) => m.name === "Plane"
        )[0].isVisible = false;
      }

      if (asset.name === "room")
        task.loadedMeshes.filter((m) => m.name === "Back")[0].isVisible = false;
    };

    assetTasks[asset.name].onError = function (task, message, exception) {
      console.log(message, exception);
    };
  }

  assetsManager.load();
  assetsManager.onProgress = function (
    remainingCount,
    totalCount,
    lastFinishedTask
  ) {
    console.log(
      "We are loading the scene. " +
        remainingCount +
        " out of " +
        totalCount +
        " items still need to be loaded."
    );
  };
};

export default assetManager;
